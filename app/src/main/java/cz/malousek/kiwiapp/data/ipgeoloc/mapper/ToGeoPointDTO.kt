package cz.malousek.kiwiapp.data.ipgeoloc.mapper

import android.text.format.DateFormat
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO

fun GeoPoint.mapToDTO(timeMillisNow: Long): GeoPointRoomDTO{
    val dateString: String =  DateFormat.format("dd/MM/yyyy", timeMillisNow ).toString()
    return GeoPointRoomDTO(
        dateString, lat, lon
    )
}
