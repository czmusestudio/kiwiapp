package cz.malousek.kiwiapp.data.flight.repository

import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.flight.dto.FlightKiwiDTO
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import io.reactivex.rxjava3.core.Single
import java.util.*

interface FlightRepository {
    fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        geoPoint: GeoPoint? = null
    ): Single<MutableList<Flight>>
}