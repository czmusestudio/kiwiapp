package cz.malousek.kiwiapp.data.flight.domain

import com.squareup.moshi.Json
import java.util.*

data class Flight (val id: String,
                   val departureTime: Long,
                   val cityFrom: String,
                   val cityTo: String,
                   val mapIdTo: String,
                   val price: String,
                   val currencyCode: String,
                   val numberOfStopOvers: Int)