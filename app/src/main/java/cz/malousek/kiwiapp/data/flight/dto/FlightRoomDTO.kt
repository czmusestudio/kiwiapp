package cz.malousek.kiwiapp.data.flight.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import cz.malousek.kiwiapp.data.flight.domain.Flight

@Entity(tableName = "flights")
data class FlightListRoomDTO(@PrimaryKey val dateString: String, val flights: MutableList<FlightRoomDTO>)

@JsonClass(generateAdapter = true)
data class FlightRoomDTO(val id: String,
                         val departureTime: Long,
                         val cityFrom: String,
                         val cityTo: String,
                         val mapIdTo: String,
                         val price: String,
                         val currencyCode: String,
                         val numberOfStopOvers: Int)
