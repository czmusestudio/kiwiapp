package cz.malousek.kiwiapp.data.flight.datasource.local

import android.text.format.DateFormat
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.db.AppDatabase
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class FlightLocalDataSourceImpl @Inject constructor(private val db: AppDatabase) :
    FlightLocalDataSource {

    override fun updateCachedFlights(flights: FlightListRoomDTO): Completable {
        return db.flightsDAO().updateCache(flights)
    }

    override fun getCachedFlights(timeMillisNow: Long): Single<FlightListRoomDTO> {
        val dateString: String = DateFormat.format("dd/MM/yyyy", timeMillisNow).toString()
        return db.flightsDAO().getCachedFlights(dateString)
    }
}