package cz.malousek.kiwiapp.data.flight.datasource.remote


import cz.malousek.kiwiapp.data.flight.dto.FlightKiwiDTO
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import io.reactivex.rxjava3.core.Single
import java.util.*

interface FlightRemoteDataSource {

    fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        geoPoint: GeoPoint
    ): Single<MutableList<FlightKiwiDTO>>

    fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        countryCode: String
    ): Single<MutableList<FlightKiwiDTO>>
}