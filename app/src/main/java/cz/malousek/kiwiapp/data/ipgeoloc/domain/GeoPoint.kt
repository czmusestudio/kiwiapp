package cz.malousek.kiwiapp.data.ipgeoloc.domain

data class GeoPoint (val lat: Double, val lon: Double)