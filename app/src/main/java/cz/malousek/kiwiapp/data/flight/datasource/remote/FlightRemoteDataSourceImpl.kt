package cz.malousek.kiwiapp.data.flight.datasource.remote

import cz.malousek.kiwiapp.data.flight.dto.FlightKiwiDTO
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject

class FlightRemoteDataSourceImpl @Inject constructor(private val flightAPI: FlightAPI) :
    FlightRemoteDataSource {

    /**
     * Fetches five popular flights to unique locations based on date and lat-lon coordinates
     * @return Single of List with 5 popular flights in form of DTO
     */
    override fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        geoPoint: GeoPoint
    ): Single<MutableList<FlightKiwiDTO>> {
        return flightAPI.getFiveDestinations(
            locale.country,
            dateFrom,
            dateTo,
            "${geoPoint.lat}-${geoPoint.lon}-250km",
            Currency.getInstance(locale).currencyCode
        ).flatMapObservable {
            Observable.fromIterable(it.data)    //emit individual flights from list in response
        }.toList()  //collect individual flights into list
    }

    /**
     * Fetches five popular flights to unique locations based on date and country code
     * @return Single of List with 5 popular flights in form of DTO
     */
    override fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        countryCode: String
    ): Single<MutableList<FlightKiwiDTO>> {


        return flightAPI.getFiveDestinations(
            locale.country,
            dateFrom,
            dateTo,
            countryCode,
            Currency.getInstance(locale).currencyCode
        ).flatMapObservable {
            Observable.fromIterable(it.data)
        }.toList()
    }


}