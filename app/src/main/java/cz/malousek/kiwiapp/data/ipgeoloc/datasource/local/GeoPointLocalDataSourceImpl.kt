package cz.malousek.kiwiapp.data.ipgeoloc.datasource.local

import android.text.format.DateFormat
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO
import cz.malousek.kiwiapp.db.AppDatabase
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GeoPointLocalDataSourceImpl @Inject constructor(private val db: AppDatabase) :
    GeoPointLocalDataSource {
    override fun updateCachedGeoPoint(geoPoint: GeoPointRoomDTO): Completable {
        return db.geoPointDAO().updateCache(geoPoint)
    }

    override fun getCachedGeoPoint(timeMillisNow: Long): Single<GeoPointRoomDTO> {
        val dateString: String = DateFormat.format("dd/MM/yyyy", timeMillisNow).toString()
        return db.geoPointDAO().getCachedGeoPoint(dateString)
    }
}