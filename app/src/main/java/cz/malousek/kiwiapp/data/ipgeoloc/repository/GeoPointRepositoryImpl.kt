package cz.malousek.kiwiapp.data.ipgeoloc.repository

import androidx.room.rxjava3.EmptyResultSetException
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.local.GeoPointLocalDataSource
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote.GeoPointRemoteDataSource
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.mapper.mapToDTO
import cz.malousek.kiwiapp.data.ipgeoloc.mapper.mapToDomain
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import javax.inject.Inject

class GeoPointRepositoryImpl @Inject constructor(
    private val remoteDataSource: GeoPointRemoteDataSource,
    private val localDataSource: GeoPointLocalDataSource
) : GeoPointRepository {

    /**
     * Tries to fetch lat-lon coordinates from local data source. If it could't, tries to fetch coordinates from
     * remote data source
     *
     * @return Single of geoPoint domain model
     */
    override fun getLocationFromDeviceIP(): Single<GeoPoint> {
        return fetchFromLocalDataSource()
            .onErrorResumeNext {
                if (it is EmptyResultSetException) {    //if local data source doesn't contain wanted geopoint try remote data source
                    Timber.d("GeoPointRepository: Local cache doesn't contain wanted geoPoint, fetching from remote dataSource...")
                    fetchFromRemoteDataSource()
                } else {
                    Single.error(it)    //Pass other errors downstream
                }
            }
    }

    private fun fetchFromLocalDataSource(): Single<GeoPoint>{
        return localDataSource
            .getCachedGeoPoint(System.currentTimeMillis())
            .map { it.mapToDomain() }
            .doOnSuccess {
                Timber.d("GeoPointRepository: GeoPoint fetched from local cache.")
            }
    }

    /**
     * Fetches GeoPoint from remote data source
     *
     * After it successfully obtains geoPoint from remote data source, it caches these data in local data source
     */
    private fun fetchFromRemoteDataSource(): Single<GeoPoint>{
        return remoteDataSource
            .getLocationFromDeviceIP()
            .map { it.mapToDomain() }
            .flatMap {
                Timber.d("GeoPointRepository: Updating local cache...")
                localDataSource.updateCachedGeoPoint(it.mapToDTO(System.currentTimeMillis()))
                    .andThen(Single.just(it))
            }.doOnSuccess {
                Timber.d("GeoPointRepository: GeoPoint fetched from remote dataSource.")
            }
    }
}