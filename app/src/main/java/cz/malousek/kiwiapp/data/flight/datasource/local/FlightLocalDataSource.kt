package cz.malousek.kiwiapp.data.flight.datasource.local

import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.data.flight.dto.FlightRoomDTO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface FlightLocalDataSource {
    fun updateCachedFlights(flights: FlightListRoomDTO): Completable
    fun getCachedFlights(timeMillisNow: Long): Single<FlightListRoomDTO>
}