package cz.malousek.kiwiapp.data.ipgeoloc.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "geo_point_cache")
data class GeoPointRoomDTO(@PrimaryKey val dateString: String, val lat: Double, val lon: Double)
