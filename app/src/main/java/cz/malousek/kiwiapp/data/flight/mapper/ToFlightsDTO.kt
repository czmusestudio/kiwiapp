package cz.malousek.kiwiapp.data.flight.mapper

import android.text.format.DateFormat
import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.data.flight.dto.FlightRoomDTO

fun Flight.mapToDTO(): FlightRoomDTO {
    return FlightRoomDTO(
        id,
        departureTime,
        cityFrom,
        cityTo,
        mapIdTo,
        price,
        currencyCode,
        numberOfStopOvers
    )
}

fun MutableList<Flight>.mapToDTO(timeMillisNow: Long): FlightListRoomDTO{
    val dateString: String =  DateFormat.format("dd/MM/yyyy", timeMillisNow ).toString()
    return FlightListRoomDTO(
        dateString,
        ArrayList(map { it.mapToDTO() })
    )
}