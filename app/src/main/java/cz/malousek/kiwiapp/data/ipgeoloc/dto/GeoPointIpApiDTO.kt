package cz.malousek.kiwiapp.data.ipgeoloc.dto

import com.squareup.moshi.Json

data class GeoPointIpApiDTO (@Json(name = "lat")var lat: Double, @Json(name = "lon")var lon: Double)