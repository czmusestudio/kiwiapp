package cz.malousek.kiwiapp.data.flight.datasource.remote

import cz.malousek.kiwiapp.data.flight.dto.FlightKiwiResponseDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface FlightAPI {

    @GET("/flights?v=3&sort=quality&asc=0&to=anywhere&featureName=aggregateResults&typeFlight=oneway&oneforcity=1&adults=1&limit=5&partner=picky")
    fun getFiveDestinations(
        @Query("locale") locale: String,
        @Query("dateFrom") dateFrom: String,
        @Query("dateTo") dateTo: String,
        @Query("flyFrom") flyFrom: String,
        @Query("curr") currency: String
    ): Single<FlightKiwiResponseDTO>


}