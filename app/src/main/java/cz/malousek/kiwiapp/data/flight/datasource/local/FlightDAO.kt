package cz.malousek.kiwiapp.data.flight.datasource.local

import androidx.room.*
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

@Dao
interface FlightDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(flightList: FlightListRoomDTO): Completable

    @Query("DELETE FROM flights")
    fun clear(): Completable

    @Query("SELECT * FROM flights WHERE dateString==:dateString")
    fun getCachedFlights(dateString: String): Single<FlightListRoomDTO>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSync(flightList: FlightListRoomDTO)

    @Query("DELETE FROM flights")
    fun clearSync()

    @Transaction
    fun clearAndInsert(flightList: FlightListRoomDTO){
        clearSync()
        insertSync(flightList)
    }

    /**
     * Clear db and then put in new flightList
     */
    fun updateCache(flightList: FlightListRoomDTO): Completable{
        return Completable.fromCallable {clearAndInsert(flightList)}
    }

}