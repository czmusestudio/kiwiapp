package cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote

import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointIpApiDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface IpApi {

    @GET("/json/?fields=lat,lon")
    fun getLocationFromDeviceIp(): Single<GeoPointIpApiDTO>
}