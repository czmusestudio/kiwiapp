package cz.malousek.kiwiapp.data.flight.mapper

import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.flight.dto.FlightKiwiDTO
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.data.flight.dto.FlightRoomDTO
import java.util.*
import kotlin.collections.ArrayList

fun FlightKiwiDTO.mapToDomain(locale: Locale): Flight{
    return Flight(
        this.id,
        this.departureTime*1000,
        this.cityFrom,
        this.cityTo,
        this.mapIdTo,
        this.price,
        Currency.getInstance(locale).currencyCode,
        route.size-1
    )
}

fun FlightListRoomDTO.mapToDomainList(): MutableList<Flight>{
    return flights
        .map { it.mapToDomain() }
        .toMutableList()
}

fun FlightRoomDTO.mapToDomain(): Flight{
    return Flight(id, departureTime, cityFrom, cityTo, mapIdTo, price, currencyCode, numberOfStopOvers)
}