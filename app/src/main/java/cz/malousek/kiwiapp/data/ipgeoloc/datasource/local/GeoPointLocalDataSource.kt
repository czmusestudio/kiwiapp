package cz.malousek.kiwiapp.data.ipgeoloc.datasource.local

import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface GeoPointLocalDataSource {
    fun updateCachedGeoPoint(geoPoint: GeoPointRoomDTO): Completable
    fun getCachedGeoPoint(timeMillisNow: Long): Single<GeoPointRoomDTO>
}