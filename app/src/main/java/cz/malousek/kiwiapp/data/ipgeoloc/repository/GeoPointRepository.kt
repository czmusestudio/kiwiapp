package cz.malousek.kiwiapp.data.ipgeoloc.repository

import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import io.reactivex.rxjava3.core.Single

interface GeoPointRepository {
    fun getLocationFromDeviceIP(): Single<GeoPoint>
}