package cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote

import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointIpApiDTO
import io.reactivex.rxjava3.core.Single

interface GeoPointRemoteDataSource {
    fun getLocationFromDeviceIP(): Single<GeoPointIpApiDTO>
}