package cz.malousek.kiwiapp.data.ipgeoloc.datasource.local

import androidx.room.*
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single


@Dao
interface GeoPointDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(geoPoint: GeoPointRoomDTO): Completable

    @Query("DELETE FROM geo_point_cache")
    fun clear():Completable

    @Query("SELECT * FROM geo_point_cache WHERE dateString==:dateString")
    fun getCachedGeoPoint(dateString: String): Single<GeoPointRoomDTO>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSync(geoPoint: GeoPointRoomDTO)

    @Query("DELETE FROM geo_point_cache")
    fun clearSync()

    @Transaction
    fun clearAndInsert(geoPoint: GeoPointRoomDTO){
        clearSync()
        insertSync(geoPoint)
    }

    /**
     * Clear db and then put in new geoPoint
     */
    fun updateCache(geoPoint: GeoPointRoomDTO): Completable{
        return Completable.fromCallable {clearAndInsert(geoPoint)}
    }


}