package cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote

import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointIpApiDTO
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GeoPointRemoteDataSourceImpl @Inject constructor(private val ipApi: IpApi) :
    GeoPointRemoteDataSource {
    override fun getLocationFromDeviceIP(): Single<GeoPointIpApiDTO> {
        return ipApi.getLocationFromDeviceIp()
    }
}