package cz.malousek.kiwiapp.data.ipgeoloc.mapper

import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointIpApiDTO
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO

fun GeoPointIpApiDTO.mapToDomain(): GeoPoint{
    return GeoPoint(lat, lon)
}

fun GeoPointRoomDTO.mapToDomain(): GeoPoint{
    return GeoPoint(lat, lon)
}