package cz.malousek.kiwiapp.data.flight.repository

import androidx.room.rxjava3.EmptyResultSetException
import cz.malousek.kiwiapp.data.flight.datasource.local.FlightLocalDataSource
import cz.malousek.kiwiapp.data.flight.datasource.remote.FlightRemoteDataSource
import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.flight.mapper.mapToDTO
import cz.malousek.kiwiapp.data.flight.mapper.mapToDomain
import cz.malousek.kiwiapp.data.flight.mapper.mapToDomainList
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.mapper.mapToDTO
import cz.malousek.kiwiapp.data.ipgeoloc.mapper.mapToDomain
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class FlightRepositoryImpl @Inject constructor(
    private val flightRemoteDataSource: FlightRemoteDataSource,
    private val flightLocalDataSource: FlightLocalDataSource
) :
    FlightRepository {

    /**
     * Tries to fetch popular locations for given date firstly from local data source (flights cached in Room db)
     * If data are not available locally, it fetches them from remote data source
     * @returns Single of list containing 5 popular flights in form of domain model.
     */
    override fun fetchFivePopularFlights(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        geoPoint: GeoPoint?
    ): Single<MutableList<Flight>> {
        return fetchFromLocalDataSource()
            .onErrorResumeNext {
                if (it is EmptyResultSetException) {    //When data are not available locally fetch them from remote data source
                    Timber.d("FlightRepository: Local cache doesn't contain wanted Flights, fetching from remote dataSource...")
                    fetchFromRemoteDataSource(locale, dateFrom, dateTo, geoPoint)
                } else {
                    Single.error(it)    // pass other errors downstream
                }
            }
    }

    /**
     * Fetches popular flight from local db
     */
    private fun fetchFromLocalDataSource(): Single<MutableList<Flight>> {
        return flightLocalDataSource
            .getCachedFlights(System.currentTimeMillis())
            .map { it.mapToDomainList() } // map dto to domain
            .doOnSuccess {
                Timber.d("FlightRepository: FlightList fetched from local cache.")
            }
    }

    /**
     * Fetches popular flight from remote data source
     * If GeoPoint is null, location is determined from Locale object - country code
     *
     * After it successfully obtains flights from remote data source, it caches these data in local data source
     */
    private fun fetchFromRemoteDataSource(
        locale: Locale,
        dateFrom: String,
        dateTo: String,
        geoPoint: GeoPoint?
    ): Single<MutableList<Flight>> {
        return flightRemoteDataSource.run {
            if (geoPoint == null) {
                this.fetchFivePopularFlights(locale, dateFrom, dateTo, locale.country)
            } else {
                this.fetchFivePopularFlights(locale, dateFrom, dateTo, geoPoint)
            }
        }
            .flatMapObservable {
                Observable.fromIterable(it)
            }
            .map { it.mapToDomain(locale) }
            .toList()
            .flatMap { flightList ->
                Timber.d("FlightRepository: Updating local cache...")
                flightLocalDataSource
                    .updateCachedFlights(flightList.mapToDTO(System.currentTimeMillis()))   //update local cache
                    .andThen(Single.just(flightList))
            }
            .doOnSuccess {
                Timber.d("FlightRepository: FlightList fetched from remote dataSource.")
            }
    }


}