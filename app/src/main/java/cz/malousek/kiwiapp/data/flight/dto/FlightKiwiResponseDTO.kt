package cz.malousek.kiwiapp.data.flight.dto

import com.squareup.moshi.Json




data class FlightKiwiResponseDTO (
    @Json(name = "search_id") var search_id: String,
    @Json(name = "data") var data: List<FlightKiwiDTO>
)

data class FlightKiwiDTO(
    @Json(name = "id") var id: String,
    @Json(name="dTimeUTC") var departureTime: Long,
    @Json(name="aTimeUTC") var arrivalTime: Long,
    @Json(name="cityFrom") var cityFrom: String,
    @Json(name="cityTo") var cityTo: String,
    @Json(name="mapIdto") var mapIdTo: String,
    @Json(name="price") var price: String,
    @Json(name="route") var route: List<RouteKiwiDTO>,
    @Json(name="conversion") var conversion: Map<String, Float>,
)

data class RouteKiwiDTO(
    @Json(name = "id") var id: String
)
