package cz.malousek.kiwiapp.util

import android.app.Application
import cz.malousek.kiwiapp.BuildConfig
import cz.malousek.kiwiapp.di.ComponentUtil
import cz.malousek.kiwiapp.di.DaggerAppComponent
import timber.log.Timber

class App : Application() {

    init {
        ComponentUtil.appComponent = DaggerAppComponent.factory().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}