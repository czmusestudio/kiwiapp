package cz.malousek.kiwiapp.util

object Constants {
    const val KIWI_BASE_URL = "https://api.skypicker.com"
    const val IP_API_BASE_URL = "http://ip-api.com"
}