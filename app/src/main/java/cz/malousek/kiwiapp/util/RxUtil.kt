package cz.malousek.kiwiapp.util

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.schedulers.Schedulers

class RxUtil {

    companion object {

        @JvmStatic
        fun <T> applyObservableIOSchedulers(): ObservableTransformer<T, T> {
            return ObservableTransformer { it.subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()) }
        }

        @JvmStatic
        fun <T> applyObservableComputationSchedulers(): ObservableTransformer<T, T> {
            return ObservableTransformer {
                it.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
            }
        }

        @JvmStatic
        fun <T> applySingleIOSchedulers(): SingleTransformer<T, T> {
            return SingleTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
        }

        @JvmStatic
        fun <T> applySingleComputationSchedulers(): SingleTransformer<T, T> {
            return SingleTransformer {
                it.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
            }
        }

        @JvmStatic
        fun <T> applyMaybeIOSchedulers(): MaybeTransformer<T, T> {
            return MaybeTransformer {
                it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            }
        }

        @JvmStatic
        fun <T> applyMaybeComputationSchedulers(): MaybeTransformer<T, T> {
            return MaybeTransformer {
                it.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
            }
        }

        @JvmStatic
        fun <T> applyFlowableSchedulers(): FlowableTransformer<T, T> {
            return FlowableTransformer {
                it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            }
        }

        @JvmStatic
        fun applyCompletableSchedulers(): CompletableTransformer {
            return CompletableTransformer {
                it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            }
        }

    }

}