package cz.malousek.kiwiapp.ui.fragments.popularflights

import androidx.lifecycle.MutableLiveData
import cz.malousek.kiwiapp.base.BaseViewModel
import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.ui.fragments.popularflights.usecases.GetFivePopularFlightsForLocationUseCase
import cz.malousek.kiwiapp.ui.fragments.popularflights.usecases.GetLocationFromDeviceIpUseCase
import cz.malousek.kiwiapp.util.Event
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class PopularFlightsViewModel @Inject constructor(
    private val getFivePopularFlightsForLocationUseCase: GetFivePopularFlightsForLocationUseCase,
    private val getLocationFromDeviceIpUseCase: GetLocationFromDeviceIpUseCase
) : BaseViewModel() {
    val lastMinuteFlights = MutableLiveData<MutableList<Flight>>()
    val errorEvents = MutableLiveData<Event<Throwable>>()

    init {
        requestData()
    }

    /**
     * Request popular flights for displaying in UI
     *
     * Data will be available via lastMinuteFlights LiveData
     * If something will go wrong, error info will be available via errorEvents Livedata
     */
    fun requestData(){
        isLoading.value = true
        getLocation()
    }


    private fun getLocation(){
        disposables.add(
            getLocationFromDeviceIpUseCase
                .invoke()
                .subscribe(
                    {
                        getDestinations(it)
                    },
                    {
                        getDestinations(null)
                    }
                )
        )
    }

    private fun getDestinations(geoPoint: GeoPoint?){
        disposables.add(
            getFivePopularFlightsForLocationUseCase
                .invoke(Locale.getDefault(), geoPoint)
                .subscribe(
                    {
                        lastMinuteFlights.value = it
                        isLoading.value = false
                    },
                    {
                        Timber.e(it)
                        isLoading.value = false
                        errorEvents.value = Event(it)
                    }
                )
        )
    }
}