package cz.malousek.kiwiapp.ui.fragments.popularflights.usecases

import android.text.format.DateFormat
import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.data.flight.repository.FlightRepository
import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.util.RxUtil
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject

class GetFivePopularFlightsForLocationUseCase @Inject constructor(
    private val flightRepository: FlightRepository
) {
    /**
     * Obtains 5 popular flights in time span 24-48 hours from now for given location.
     * Location is given by Geopoint. If geopoint is null, location is determined from locale country code
     * @return Single of Mutable List with 5 flights
     */
    fun invoke(locale: Locale, geoPoint: GeoPoint?): Single<MutableList<Flight>> {
        val startDateMillis = System.currentTimeMillis() + 24*60*60*1000
        val endDateMillis = System.currentTimeMillis() + 2*24*60*60*1000

        return flightRepository
            .fetchFivePopularFlights(
                locale,
                DateFormat.format("dd/MM/yyyy", startDateMillis ).toString(),
                DateFormat.format("dd/MM/yyyy", endDateMillis ).toString(),
                geoPoint
            )
            .compose(RxUtil.applySingleIOSchedulers()) //Apply schedulers for subscribing and observing
    }

}