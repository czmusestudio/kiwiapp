package cz.malousek.kiwiapp.ui.fragments.popularflights.usecases

import cz.malousek.kiwiapp.data.ipgeoloc.domain.GeoPoint
import cz.malousek.kiwiapp.data.ipgeoloc.repository.GeoPointRepository
import cz.malousek.kiwiapp.util.RxUtil
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetLocationFromDeviceIpUseCase @Inject constructor(private val geoPointRepository: GeoPointRepository) {

    /**
     * Obtains GeoPoint of device without permission to access device location
     * @return Single of GeoPoint
     */
    fun invoke(): Single<GeoPoint> {
        return geoPointRepository
            .getLocationFromDeviceIP()
            .compose(RxUtil.applySingleIOSchedulers()) //Apply schedulers for subscribing and observing
    }

}