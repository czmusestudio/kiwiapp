package cz.malousek.kiwiapp.ui.fragments.popularflights

import androidx.recyclerview.widget.DiffUtil
import cz.malousek.kiwiapp.data.flight.domain.Flight

class FlightDiffCallBack(private val oldList: List<Flight>,
                         private val newList: List<Flight>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}