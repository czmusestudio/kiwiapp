package cz.malousek.kiwiapp.ui.fragments.popularflights

import android.annotation.SuppressLint
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import cz.malousek.kiwiapp.R
import cz.malousek.kiwiapp.data.flight.domain.Flight
import cz.malousek.kiwiapp.databinding.ItemFlightBinding
import javax.inject.Inject
import kotlin.math.roundToInt

class FlightAdapter @Inject constructor(private val layoutInflater: LayoutInflater) : RecyclerView.Adapter<FlightViewHolder>() {

    private val dataSet = ArrayList<Flight>()

    fun update(newDataSet: ArrayList<Flight>){
        val diffCallback = FlightDiffCallBack(this.dataSet, newDataSet)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        dataSet.clear()
        dataSet.addAll(newDataSet)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = dataSet.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
        val binding = ItemFlightBinding.inflate(layoutInflater, parent, false)
        return FlightViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }
}

class FlightViewHolder(private val binding: ItemFlightBinding) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n")
    fun bind(flight: Flight){

        binding.tvDestinationCity.text = flight.cityTo
        binding.tvPrice.text = "${flight.price} ${flight.currencyCode}"
        binding.tvWhence.text = flight.cityFrom
        binding.tvDeparture.text = DateFormat.format("dd/MM/yyyy hh:mm", flight.departureTime).toString()
        binding.tvStopOvers.text = if(flight.numberOfStopOvers < 1) binding.root.context.getString(R.string.none_direct_flight)
            else flight.numberOfStopOvers.toString()

        val cornerRadius = binding.root.context.resources.getDimension(R.dimen.corner_radius).roundToInt()
        Glide
            .with(binding.root)
            .load("https://images.kiwi.com/photos/472x440/${flight.mapIdTo}.jpg")
            .placeholder(R.drawable.non_existing)
            .apply(RequestOptions.bitmapTransform(RoundedCorners(cornerRadius)))
            .into(binding.ivDestination)
    }
}