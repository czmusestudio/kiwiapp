package cz.malousek.kiwiapp.ui.fragments.popularflights

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.yarolegovich.discretescrollview.DSVOrientation
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import cz.malousek.kiwiapp.R
import cz.malousek.kiwiapp.databinding.FragmentPopularFlightsBinding
import cz.malousek.kiwiapp.di.ComponentUtil
import cz.malousek.kiwiapp.util.ViewModelFactory
import javax.inject.Inject
import kotlin.math.absoluteValue

class PopularFlightsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var flightAdapter: FlightAdapter

    lateinit var binding: FragmentPopularFlightsBinding

    private val viewModel: PopularFlightsViewModel by viewModels { viewModelFactory }

    private val scrollStateChangeListener = object :
        DiscreteScrollView.ScrollStateChangeListener<FlightViewHolder> {
        override fun onScrollStart(currentItemHolder: FlightViewHolder, adapterPosition: Int) {
        }

        override fun onScrollEnd(currentItemHolder: FlightViewHolder, adapterPosition: Int) {
            //Set PageIndicator view to current position after scrolling ends (insurance so it will display correct page)
            binding.piv.selection = adapterPosition
        }

        override fun onScroll(
            scrollPosition: Float,
            currentPosition: Int,
            newPosition: Int,
            currentHolder: FlightViewHolder?,
            newCurrent: FlightViewHolder?
        ) {
            //display change of position in PageIndicatorView when items are at 40 % between switch to another one
            if (scrollPosition.absoluteValue > 0.4f)
                binding.piv.selection = newPosition
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ComponentUtil.appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPopularFlightsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        observeData()
    }

    private fun initUI() {
        //recyclerview with snapping behavior. Viewpager wasn't used because I wanted to display multiple items in tablets
        binding.rvFlights.apply {
            setOrientation(DSVOrientation.HORIZONTAL)
            adapter = flightAdapter
            setItemTransformer(
                ScaleTransformer.Builder()
                    .setMinScale(0.85f)
                    .build()
            )
        }

        binding.piv.count = 5
    }

    override fun onStart() {
        super.onStart()
        binding.rvFlights.addScrollStateChangeListener(scrollStateChangeListener)
    }

    override fun onStop() {
        super.onStop()
        binding.rvFlights.removeScrollStateChangeListener(scrollStateChangeListener)
    }

    private fun observeData() {
        viewModel.lastMinuteFlights.observe(viewLifecycleOwner){
            flightAdapter.update(ArrayList(it))
        }

        viewModel.errorEvents.observe(viewLifecycleOwner){
            it.getContentIfNotHandled()?.let { displayError() }
        }
    }

    private fun displayError(){
        val alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setTitle(R.string.something_went_wrong)
        alertBuilder.setMessage(R.string.error_messsage)
        alertBuilder.setPositiveButton(R.string.try_again) { _: DialogInterface, _: Int ->
            viewModel.requestData()
        }
        alertBuilder.show()
    }
}