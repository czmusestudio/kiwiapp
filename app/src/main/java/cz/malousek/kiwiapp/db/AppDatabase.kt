package cz.malousek.kiwiapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import cz.malousek.kiwiapp.data.flight.datasource.local.FlightDAO
import cz.malousek.kiwiapp.data.flight.dto.FlightListRoomDTO
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.local.GeoPointDAO
import cz.malousek.kiwiapp.data.ipgeoloc.dto.GeoPointRoomDTO


@Database(
    entities = [GeoPointRoomDTO::class, FlightListRoomDTO::class], version = 3
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun geoPointDAO(): GeoPointDAO
    abstract fun flightsDAO():FlightDAO
}