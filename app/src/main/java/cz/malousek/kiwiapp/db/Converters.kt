package cz.malousek.kiwiapp.db

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import cz.malousek.kiwiapp.data.flight.dto.FlightRoomDTO
import java.lang.reflect.ParameterizedType
import kotlin.collections.ArrayList


class Converters {
    private val moshi = Moshi.Builder().build()
    private val listType: ParameterizedType = Types.newParameterizedType(List::class.java, FlightRoomDTO::class.java)
    private val adapter: JsonAdapter<MutableList<FlightRoomDTO>> = moshi.adapter(listType)


    //For simplicity - list of flights are saved in room as string converted by Moshi
    @TypeConverter
    fun fromArrayListOfFlights(flights: MutableList<FlightRoomDTO>): String{
        return adapter.toJson(flights)
    }

    @TypeConverter
    fun toArrayListOfFlights(json: String): MutableList<FlightRoomDTO> {
        return adapter.fromJson(json) ?: ArrayList()
    }
}