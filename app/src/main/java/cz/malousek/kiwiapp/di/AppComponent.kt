package cz.malousek.kiwiapp.di

import cz.malousek.kiwiapp.di.modules.*
import cz.malousek.kiwiapp.ui.fragments.popularflights.PopularFlightsFragment
import cz.malousek.kiwiapp.util.App
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelModule::class, NetworkModule::class, RepositoryModule::class, DataSourceModule::class])
interface AppComponent {
    fun inject(popularFlightsFragment: PopularFlightsFragment)


    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: App) : AppComponent
    }
}