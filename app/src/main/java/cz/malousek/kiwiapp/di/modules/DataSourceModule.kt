package cz.malousek.kiwiapp.di.modules

import androidx.annotation.NonNull
import cz.malousek.kiwiapp.data.flight.datasource.local.FlightLocalDataSource
import cz.malousek.kiwiapp.data.flight.datasource.local.FlightLocalDataSourceImpl
import cz.malousek.kiwiapp.data.flight.datasource.remote.FlightRemoteDataSource
import cz.malousek.kiwiapp.data.flight.datasource.remote.FlightRemoteDataSourceImpl
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.local.GeoPointLocalDataSource
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.local.GeoPointLocalDataSourceImpl
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote.GeoPointRemoteDataSource
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote.GeoPointRemoteDataSourceImpl
import dagger.Binds
import dagger.Module

@Module
abstract class DataSourceModule {
    @Binds
    @NonNull
    abstract fun flightRemoteDataSource(flightRemoteDataSourceImpl: FlightRemoteDataSourceImpl): FlightRemoteDataSource

    @Binds
    @NonNull
    abstract fun flightsLocalDataSource(flightLocalDataSourceImpl: FlightLocalDataSourceImpl): FlightLocalDataSource

    @Binds
    @NonNull
    abstract fun geoPointRemoteDataSource(geoLocRemoteDataSourceImpl: GeoPointRemoteDataSourceImpl): GeoPointRemoteDataSource

    @Binds
    @NonNull
    abstract fun geoPointLocalDataSource(geoPointLocalDataSourceImpl: GeoPointLocalDataSourceImpl): GeoPointLocalDataSource
}