package cz.malousek.kiwiapp.di.modules

import android.content.Context
import android.view.LayoutInflater
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import cz.malousek.kiwiapp.db.AppDatabase
import cz.malousek.kiwiapp.util.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(app: App): Context = app.applicationContext

    @Provides
    fun provideLayoutInflater(context: Context): LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @Singleton
    @Provides
    fun provideRoomDB(context: Context): AppDatabase = Room
        .databaseBuilder(context, AppDatabase::class.java, "AppDB")
        .fallbackToDestructiveMigration()
        .build()

}