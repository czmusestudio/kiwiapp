package cz.malousek.kiwiapp.di.modules

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import cz.malousek.kiwiapp.data.flight.datasource.remote.FlightAPI
import cz.malousek.kiwiapp.data.ipgeoloc.datasource.remote.IpApi
import cz.malousek.kiwiapp.util.Constants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi
        .Builder()
        .add(KotlinJsonAdapterFactory())
        .build()


    @Singleton
    @Named("kiwi")
    @Provides
    fun provideRetrofitKiwiInstance(moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.KIWI_BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    fun provideFlightApi(@Named("kiwi")retrofit: Retrofit): FlightAPI = retrofit.create(FlightAPI::class.java)

    @Singleton
    @Named("ipapi")
    @Provides
    fun provideRetrofitIpApiInstance(moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.IP_API_BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    fun provideIpApi(@Named("ipapi")retrofit: Retrofit): IpApi = retrofit.create(IpApi::class.java)
}