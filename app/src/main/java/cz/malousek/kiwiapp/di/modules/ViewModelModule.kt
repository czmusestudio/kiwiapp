package cz.malousek.kiwiapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.malousek.kiwiapp.ui.fragments.popularflights.PopularFlightsViewModel
import cz.malousek.kiwiapp.util.ViewModelFactory
import cz.malousek.kiwiapp.util.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule{
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PopularFlightsViewModel::class)
    internal abstract fun destinationsViewModel(viewModel: PopularFlightsViewModel): ViewModel

}