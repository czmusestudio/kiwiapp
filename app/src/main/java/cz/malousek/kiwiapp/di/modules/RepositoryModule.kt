package cz.malousek.kiwiapp.di.modules

import androidx.annotation.NonNull
import cz.malousek.kiwiapp.data.flight.repository.FlightRepository
import cz.malousek.kiwiapp.data.flight.repository.FlightRepositoryImpl
import cz.malousek.kiwiapp.data.ipgeoloc.repository.GeoPointRepository
import cz.malousek.kiwiapp.data.ipgeoloc.repository.GeoPointRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    @Binds
    @NonNull
    abstract fun flightRepository(flightRepositoryImpl: FlightRepositoryImpl): FlightRepository

    @Binds
    @NonNull
    abstract fun geoLocRepository(geoPointRepositoryImpl: GeoPointRepositoryImpl): GeoPointRepository
}